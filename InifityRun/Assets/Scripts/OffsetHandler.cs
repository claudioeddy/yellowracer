﻿using UnityEngine;
using System.Collections;

public class OffsetHandler : MonoBehaviour
{
	private Material currentMaterial;
	public float speed;
	private float offset;

	void Start ()
	{
		currentMaterial = GetComponent<Renderer>().material;
	}
	
	void Update ()
	{
		offset += 0.01f;
		currentMaterial.SetTextureOffset("_MainTex",new Vector2(offset*speed,0));
	}
}
