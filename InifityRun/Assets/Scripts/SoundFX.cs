﻿using UnityEngine;
using System.Collections;

public enum sound {
	COIN, JUMP
}

public class SoundFX : MonoBehaviour
{
	public AudioClip coin, jump;
	public AudioSource audio;
	public static SoundFX instance;

	void Start ()
	{
		audio = GetComponent<AudioSource>();
		instance = this;
	}

	public static void PlaySound(sound currentSound)
	{
		switch (currentSound) 
		{
			case sound.COIN:
				instance.audio.PlayOneShot(instance.coin, 1f);
				break;
			case sound.JUMP:
				instance.audio.PlayOneShot(instance.jump, 0.5f);
				break;
		}
	}
}
