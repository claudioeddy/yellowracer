﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
	/* Initial buttons */
	public GameObject startButton;
	public GameObject quitButton;
	
	/* Control && status panel */
	public Text scoreTxt;
	private float score;
	public Transform progressBar;
	public GameObject prgBarObj;
	public GameObject prgFinishObj;
	public GameObject padObj;
	private float prgBarMaxScale;
	public float levelDuration;
	public static bool isLevelOver;
	private float updateDuration;

	private SpawnHandler spawner;
	private bool gameStarted;
	public AudioSource audio;

	/* Enable/disable control && status panel */
	void StartComponents(bool enable)
	{
		scoreTxt.enabled = enable;
		prgBarObj.SetActive(enable);
		prgFinishObj.SetActive(enable);
		padObj.SetActive(enable);
		startButton.SetActive(!enable);
		quitButton.SetActive(!enable);
		
		if(enable)
		{
			spawner.Spawn();
			gameStarted = true;
			audio.Play();
		}
	}

	/* This method is called via the play button */
	public void Play()
	{
		StartComponents(true);
	}

	/* This method is called via the quit button */
	public void Quit()
	{
		Application.Quit();
	}

	void SavePrgBarMaxSize()
	{
		Vector3 localScale = progressBar.localScale;
		progressBar.localScale = new Vector3(0, localScale.y, localScale.z);
	}

	void Start ()
	{
		spawner = FindObjectOfType(typeof(SpawnHandler)) as SpawnHandler;
		prgBarMaxScale = progressBar.localScale.x;
		isLevelOver = false;
		
		SavePrgBarMaxSize();
		int fromGameOver = PlayerPrefs.GetInt("fromGameOver");
		
		if(fromGameOver == 1)
		{
			StartComponents(true);
		}
		else {
			StartComponents(false);
		}
		
		PlayerPrefs.SetInt("fromGameOver",0);
	}

	void UpdateScore()
	{
		if(!isLevelOver)
		{
			score += Time.deltaTime;
		}
		scoreTxt.text = Mathf.RoundToInt(score).ToString() + " points";
	}

	public void SaveScore()
	{
		PlayerPrefs.SetInt("score",Mathf.RoundToInt(score));
		UpdateRecord();
	}

	void UpdatePrgBar(float deltaTime)
	{
		updateDuration += deltaTime;
		float percentage = (updateDuration/levelDuration)*prgBarMaxScale;

		if(percentage <= prgBarMaxScale)
		{
			progressBar.localScale = new Vector3(percentage, progressBar.localScale.y, progressBar.localScale.z);
		}
		else if (!isLevelOver) {
			isLevelOver = true;
		}
	}


	void Update ()
	{
		if(gameStarted)
		{
			UpdateScore();

			UpdatePrgBar(Time.deltaTime);			
		}
	}

	void UpdateRecord()
	{
		if(Mathf.RoundToInt(score) > PlayerPrefs.GetInt("record"))
		{
			PlayerPrefs.SetInt("record", Mathf.RoundToInt(score));
		}
	}

	void OnCharInvisible()
	{
		if(!isLevelOver)
		{
			UpdateRecord();
			Application.LoadLevel("GameOver");
		}
		SaveScore();
	}

	void ScoreUp()
	{
		score += 5;
	}
}
