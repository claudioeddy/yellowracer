﻿using UnityEngine;
using System.Collections;

public class TouchHandler : MonoBehaviour
{
	private Camera cam;

	void Start ()
	{
		cam = GetComponent<Camera>();
	}
	
	void Update ()
	{
		if(Input.GetButtonDown("Cancel"))
		{
			Application.Quit();
		}

		if(Input.touchCount > 0)
		{
			foreach(Touch touch in Input.touches)
			{
				Ray ray = cam.ScreenPointToRay(touch.position);
				RaycastHit hit;

				if(Physics.Raycast(ray, out hit))
				{
					GameObject obj = hit.transform.gameObject;

					switch(touch.phase)
					{
						case TouchPhase.Began:
							obj.SendMessage("Began",SendMessageOptions.DontRequireReceiver);
							break;
						case TouchPhase.Ended:
							obj.SendMessage("Ended",SendMessageOptions.DontRequireReceiver);
							break;
						case TouchPhase.Stationary:
							obj.SendMessage("Stationary",SendMessageOptions.DontRequireReceiver);
							break;
						case TouchPhase.Moved:
							obj.SendMessage("Moved",SendMessageOptions.DontRequireReceiver);
							break;
						case TouchPhase.Canceled:
							obj.SendMessage("Canceled",SendMessageOptions.DontRequireReceiver);
							break;
					}
				} 
			}
		}
	}
}
