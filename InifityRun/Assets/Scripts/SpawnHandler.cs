﻿using UnityEngine;
using System.Collections;

public class SpawnHandler : MonoBehaviour
{
	public GameObject[] prefab;
	public GameObject prefabF;

	public void Spawn()
	{
		GameObject tempPrefab = Instantiate(prefab[Random.Range(0,prefab.Length)]) as GameObject;
		float y = tempPrefab.transform.position.y;
		tempPrefab.transform.position = new Vector3(transform.position.x, y, transform.position.z);
	}

	public void SpawnFinish()
	{
		GameObject tempPrefab = Instantiate(prefabF) as GameObject;
		float y = tempPrefab.transform.position.y;
		tempPrefab.transform.position = new Vector3(transform.position.x, y, transform.position.z);
	}
}
