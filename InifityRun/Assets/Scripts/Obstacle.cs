﻿using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour
{
	public float movSpeed;
	private float xPos;
	private bool isSpawned;
	private Transform respawnPos;

	private SpawnHandler spawner; 

	void Start ()
	{
		respawnPos = GameObject.Find("ObjDestroyPos").transform;
		spawner = FindObjectOfType(typeof(SpawnHandler)) as SpawnHandler;
	}
	
	void Update ()
	{
		xPos = transform.position.x;
		xPos += movSpeed*Time.deltaTime;

		if(!isSpawned && transform.position.x <= 0)
		{
			if(!GameController.isLevelOver)
			{
				spawner.Spawn();
			}
			else
			{
				spawner.SpawnFinish();
			}
			isSpawned = true;
		}

		transform.position = new Vector3(xPos, transform.position.y, transform.position.z);

		if(transform.position.x < respawnPos.position.x)
		{
			Destroy(this.gameObject);
		}
	}
}
