﻿using UnityEngine;
using System.Collections;

public class MainChar : MonoBehaviour 
{
	private Rigidbody2D mainChar;
	private Transform groundCheck;
	public bool isOnGround;
	private Animator charAnimation;
	public float jumpForce;
	public float movSpeed;
	public  float totalMovSpeed;
	public bool isOnWall;
	public float charMovAxis;
	public GameObject GC;

	void Start ()
	{
		mainChar = GetComponent<Rigidbody2D>();
		groundCheck = GameObject.Find("groundCheck").transform;
		charAnimation = GetComponent<Animator>();
	}
	
	void Update ()
	{
		UpdateCharOnGround();
		UpdateCharMovSpeed();
		/* use keyboard to jump if not on target */
#if UNITY_EDITOR
		if(Input.GetButtonDown("Jump"))
		{
			Jump();
		}
#endif
	}

	void UpdateCharMovSpeed()
	{
		/* if not running on target, use keyboard */		
#if UNITY_EDITOR
		charMovAxis = Input.GetAxis("Horizontal");
#endif
		if(charMovAxis < 0)
		{
			totalMovSpeed = isOnWall ? movSpeed + 1 : movSpeed + 2;
		}
		else {
			totalMovSpeed = movSpeed;
		}

		if(!isOnWall)
		{
			mainChar.velocity = new Vector2(charMovAxis*totalMovSpeed,mainChar.velocity.y);
		}
	}

	void UpdateCharOnGround()
	{
		isOnGround = Physics2D.OverlapCircle(groundCheck.position, 0.02f);
		charAnimation.SetBool("onGround",isOnGround);
	}

	public void Jump()
	{
		if(isOnGround)
		{
			mainChar.AddForce(new Vector2(0,jumpForce));
			SoundFX.PlaySound(sound.JUMP);
		}
	}

	void OnCoinCollision(Collider2D coin)
	{
		Destroy(coin.gameObject);
		SoundFX.PlaySound(sound.COIN);
		GC.SendMessage("ScoreUp",SendMessageOptions.DontRequireReceiver);
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		switch(col.gameObject.tag)
		{
			case "coin":
				OnCoinCollision(col);
				break;
			case "finish":
				Application.LoadLevel("GameOver");
				GC.SendMessage("SaveScore",SendMessageOptions.DontRequireReceiver);
				break;
		}
	}

	void OnTriggerStay2D(Collider2D col)
	{ 
		isOnWall = !col.isTrigger;
	}

	void OnTriggerExit2D(Collider2D col)
	{
		isOnWall = col.isTrigger;
	}

	void OnBecameInvisible()
	{
		Vector3 objectPos = Camera.main.WorldToViewportPoint(transform.position);
		if(objectPos.x <= 0f)
		{
			GC.SendMessage("OnCharInvisible",SendMessageOptions.DontRequireReceiver);
		}
	}
}
