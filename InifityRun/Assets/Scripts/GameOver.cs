﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
	public Text scoreTxt;
	public Text recordTxt;
	public Text youWinLoseTxt;
	public GameObject playerOver;
	public GameObject playerWin;

	void Start ()
	{
		int score = PlayerPrefs.GetInt("score");
		int record = PlayerPrefs.GetInt("record");

		if(GameController.isLevelOver)
		{
			youWinLoseTxt.text = "You Win!";
			playerWin.SetActive(true);
			playerOver.SetActive(false);
		}
		else
		{
			youWinLoseTxt.text = "You Lose!";
			playerWin.SetActive(false);			
			playerOver.SetActive(true);
		}

		youWinLoseTxt.text += record == score ? " New Record!" : "";

		scoreTxt.text = "Score: "+score;
		recordTxt.text = "Record: "+record;
	}
	
	void Update ()
	{
		if(Input.GetButtonDown("Cancel"))
		{
			Application.Quit();
		}
	}

	public void Quit()
	{
		Application.Quit();
	}

	public void Play()
	{
		Application.LoadLevel("GameOn");
		PlayerPrefs.SetInt("fromGameOver",1);
	}
}
