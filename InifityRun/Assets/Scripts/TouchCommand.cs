﻿using UnityEngine;
using System.Collections;

public class TouchCommand : MonoBehaviour {
	public string buttonName;
	public MainChar GC;
	private float sensitivity;

	void Start ()
	{
		buttonName = this.gameObject.name;
		GC = FindObjectOfType(typeof(MainChar)) as MainChar;
		sensitivity = 3f;
	}
	
	void Began()
	{
		switch(buttonName)
		{
			case "Up":
				break;
			case "Down":
				break;
			case "Left":
				GC.charMovAxis = Mathf.Lerp(GC.charMovAxis, -1, Time.deltaTime*sensitivity);
				break;
			case "Right":
				GC.charMovAxis = Mathf.Lerp(GC.charMovAxis, 1, Time.deltaTime*sensitivity);
				break;
			case "A":
				break;
			case "B":
				GC.Jump();
				break;
		}
	}
	void Ended()
	{
		switch(buttonName)
		{
			case "Up":
				break;
			case "Down":
				break;
			case "Left":
				GC.charMovAxis = 0;
				break;
			case "Right":
				GC.charMovAxis = 0;
				break;
			case "A":
				break;
			case "B":
				break;
		}
	}
	void Stationary()
	{
		switch(buttonName)
		{
			case "Up":
				break;
			case "Down":
				break;
			case "Left":
				GC.charMovAxis = Mathf.Lerp(GC.charMovAxis, -1, Time.deltaTime*sensitivity);
				break;
			case "Right":
				GC.charMovAxis = Mathf.Lerp(GC.charMovAxis, 1, Time.deltaTime*sensitivity);
				break;
			case "A":
				break;
			case "B":
				break;
		}
	}

	void Moved()
	{
		switch(buttonName)
		{
			case "Up":
				break;
			case "Down":
				break;
			case "Left":
				break;
			case "Right":
				break;
			case "A":
				break;
			case "B":
				break;
		}
	}

	void Canceled()
	{
		switch(buttonName)
		{
			case "Up":
				break;
			case "Down":
				break;
			case "Left":
				break;
			case "Right":
				break;
			case "A":
				break;
			case "B":
				break;
		}
	}
}
